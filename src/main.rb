require_relative 'rect'
require_relative 'area'

def find_largest_rect_from_point(area, point_x, point_y, value = "F")
  return nil if area[point_x, point_y] != value

  current_rect = Rect.new(point_x, point_y, 1, 1)

  best_rect = current_rect
  max_y = area.bounds.bottom

  for x in point_x..area.bounds.right do
    break if area[x, point_y] != value
    for y in point_y + 1..max_y do
      if area[x, y] != value
        max_y = y - 1
        break
      end
    end

    new_rect = Rect.new(point_x, point_y, x - point_x + 1, max_y - point_y + 1)
    best_rect = new_rect if new_rect.area > best_rect.area
  end

  best_rect
end

def find_largest_rect(area, value = "F")
  best_rect = nil

  area.bounds.each do |x, y|
    point_rect = find_largest_rect_from_point area, x, y, value
    best_rect = point_rect if best_rect.nil? || !point_rect.nil? && point_rect.area > best_rect.area
  end

  best_rect
end

if __FILE__ == $0
  input = "input1.txt"
  unit_price = 3
  areas = Area.all_from_lines File.readlines input

  for area in areas do
    # puts area
    largest_rect = find_largest_rect area
    # puts largest_rect
    puts largest_rect.nil? ? 0 : largest_rect.area * unit_price
  end
end