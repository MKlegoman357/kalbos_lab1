require_relative 'rect'

class Area
  attr_reader :width, :height

  def initialize(width, height, units = nil)
    @width = width
    @height = height
    @units = units || Array.new(height) { Array.new(width, "U") }
  end

  def bounds
    Rect.new(0, 0, @width, @height)
  end

  def check_area(rect, value)
    return false unless rect.inside? self.bounds
    rect.each { |x, y| return false if @units[y][x] != value }
    true
  end

  def to_s
    "width=#{@width}, height=#{@height}\n" + @units.map { |row| row.join(" ") }.join("\n")
  end

  def [](x, y)
    raise ArgumentError, "Invalid coords x=#{x} y=#{y} (width=#{@width} height=#{@height})" if (x < 0 || x >= @width || y < 0 || y >= @height)
    @units[y][x]
  end

  def []=(x, y, value)
    raise ArgumentError, "Invalid coords x=#{x} y=#{y} (width=#{@width} height=#{@height})" if (x < 0 || x >= @width || y < 0 || y >= @height)
    raise ArgumentError, "Invalid value #{value}, must be single char" if value.size != 1
    @units[y][x] = value
  end

  def self.from_lines(lines)
    height, width = lines[0].split(" ").map { |n| n.to_i }
    units = Array.new(height) { |index| lines[index + 1].split(" ") }
    return Area.new(width, height, units)
  end

  def self.all_from_lines(lines)
    area_count = lines[0].to_i
    areas = []
    last_line = 1

    (0..area_count - 1).each do
      areas.push area = Area.from_lines(lines[last_line..-1])
      last_line += area.height + 1
      last_line += 1 if last_line < lines.length && lines[last_line].chomp.size == 0
    end

    return areas
  end
end