class Rect
  attr_reader :x, :y, :width, :height, :right, :bottom

  def initialize(x, y, width, height)
    @x = x
    @y = y
    @width = width
    @height = height
    @right = x + width - 1
    @bottom = y + height - 1
  end

  def area
    @width * @height
  end

  def to_s
    "rect x=#{@x} y=#{@y} width=#{@width} height=#{@height}"
  end

  def includes?(x, y)
    x >= @x && x <= @right && y >= @y && y <= @bottom
  end

  def intersects?(other)
    @right >= other.x && @x <= other.right && @bottom >= other.y && @y <= other.bottom
  end

  def inside?(other)
    @x >= other.x && @right <= other.right && @y >= other.y && @bottom <= other.bottom
  end

  def each
    for y in self.y..self.bottom do
      for x in self.x..self.right do
        yield x, y
      end
    end
  end
end